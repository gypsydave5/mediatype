package mediatype_test

import (
	"fmt"
	"reflect"
	"testing"

	m "gitlab.com/gypsydave5/mediatype"
)

func TestParseSimpleMediaType(t *testing.T) {
	mediatypeString := "application/json"
	mt, err := m.ParseMediaType(mediatypeString)
	expected := m.NewMediaType("application", "json")
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if !reflect.DeepEqual(mt, expected) {
		t.Errorf("wanted %v, got %v", expected, mt)
	}
}

func TestParseSimpleMediaTypeParameters(t *testing.T) {
	mediatypeString := "application/json;x=1;y=hello"
	mt, err := m.ParseMediaType(mediatypeString)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	expectedParameters := map[string]string{"x": "1", "y": "hello"}
	if !reflect.DeepEqual(mt.Parameters(), expectedParameters) {
		t.Errorf("wanted %v, got %v", expectedParameters, mt.Parameters())
	}
}

func TestParseDeformedMediaType(t *testing.T) {
	deformed := "application/json/jupiter"
	mt, err := m.ParseMediaType(deformed)
	if err == nil {
		t.Errorf("expected an error, but there was none")
	}

	expected := m.NewMediaType("application", "json")
	if !reflect.DeepEqual(mt, expected) {
		t.Errorf("wanted %v, got %v", expected, mt)
	}
}

func TestParseAcceptMediaType(t *testing.T) {
	s := "application/json;q=0.8"
	amt, err := m.ParseMediaType(s)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if amt.Type() != "application" {
		t.Errorf("Expected Type of 'application', got %v", amt.Type())
	}
	if amt.Subtype() != "json" {
		t.Errorf("Expected Subtype of 'json', got %v", amt.Subtype())
	}
	if amt.Quality() != 0.8 {
		t.Errorf("Expected Quality  of '0.8', got %v", amt.Quality())
	}
}

func TestParseAcceptMediaTypeDefaultQ(t *testing.T) {
	s := "text/plain"
	amt, err := m.ParseMediaType(s)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	if amt.Quality() != 1.0 {
		t.Errorf("wanted %v, got %v", 1.0, amt.Quality())
	}
}

func TestParseAcceptMediaTypes(t *testing.T) {
	s := "*/*;q=0.22,text/plain,text/*;q=0.6,text/html,text/bob;q=0.8"
	amts, err := m.ParseMediaTypes(s)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	expected := []m.MediaType{
		m.NewMediaType("text", "plain"),
		m.NewMediaType("text", "html"),
		m.NewMediaTypeWithParams("text", "bob", map[string]string{"q": "0.8"}),
		m.NewMediaTypeWithParams("text", "*", map[string]string{"q": "0.6"}),
		m.NewMediaTypeWithParams("*", "*", map[string]string{"q": "0.22"}),
	}

	for i := 0; i < len(expected); i++ {
		if !mtEqual(expected[i], amts[i]) {
			t.Errorf("Wanted %v, got %v", expected[i], amts[i])
		}
	}
}

func TestMediaTypeString(t *testing.T) {
	mt := m.NewMediaType("foo", "bar")
	s := fmt.Sprintf("%s", mt)
	expected := "foo/bar"
	if s != expected {
		t.Errorf("Expected %v got %v", expected, s)
	}
}

func mtEqual(mt1, mt2 m.MediaType) bool {
	return mt1.Type() == mt2.Type() && mt1.Subtype() == mt2.Subtype() &&
		paramsEqual(mt1.Parameters(), mt2.Parameters())
}

func paramsEqual(p1, p2 map[string]string) bool {
	for k1, v1 := range p1 {
		if p2[k1] != v1 {
			return false
		}
	}

	for k2, v2 := range p1 {
		if p1[k2] != v2 {
			return false
		}
	}

	return true
}
