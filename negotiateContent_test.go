package mediatype_test

import (
	m "gitlab.com/gypsydave5/mediatype"
	"testing"
)

var negotiateContentTests = []struct {
	name   string
	amts   m.MediaTypes
	mts    m.MediaTypes
	expect m.MediaType
}{
	{
		"everything empty",
		[]m.MediaType{},
		[]m.MediaType{},
		m.NoMatch,
	},
	{
		"no matches",
		[]m.MediaType{m.NewAcceptMediaType("text", "plain", 1.0)},
		[]m.MediaType{m.NewMediaType("application", "json")},
		m.NoMatch,
	},
	{
		"simple negotiation",
		[]m.MediaType{
			m.NewAcceptMediaType("text", "bob", 1.0),
			m.NewAcceptMediaType("text", "joe", 1.0),
			m.NewAcceptMediaType("text", "gary", 1.0),
		},
		[]m.MediaType{
			m.NewMediaType("text", "wilma"),
			m.NewMediaType("text", "joe"),
			m.NewMediaType("audio", "fun"),
		},
		m.NewMediaType("text", "joe"),
	},
	{
		"wildcard match",
		[]m.MediaType{m.NewAcceptMediaType("audio", "*", 1.0)},
		[]m.MediaType{m.NewMediaType("bob", "*"), m.NewMediaType("audio", "mp3")},
		m.NewMediaType("audio", "mp3"),
	},
	{
		"any content match",
		[]m.MediaType{
			m.NewMediaTypeWithParams("*", "*", map[string]string{"q": "0.1"}),
			m.NewMediaType("poop", "*"),
		},
		[]m.MediaType{m.NewMediaType("text", "html")},
		m.NewMediaType("text", "html"),
	},
	{
		"unsorted accepts",
		[]m.MediaType{
			m.NewMediaTypeWithParams("text", "html", map[string]string{"q": "0.1"}),
			m.NewMediaType("text", "plain"),
		},
		[]m.MediaType{
			m.NewMediaType("text", "html"),
			m.NewMediaType("text", "plain"),
		},
		m.NewMediaType("text", "plain"),
	},
}

func TestNegotiateContent(t *testing.T) {
	for _, test := range negotiateContentTests {
		t.Run(test.name, func(t *testing.T) {
			actual := test.amts.Negotiate(test.mts)
			if !mtEqual(actual, test.expect) {
				t.Errorf("got %v, want %v", actual, test.expect)
			}
		})
	}
}

func TestNegotiateContentStrings(t *testing.T) {
	m.NegotiateContent(
		"text/html;q=0.5;text/plain,text/*;q=0.3,audio/*;q=0.1",
		m.MediaTypes{
			m.NewMediaType("audio", "mp3"),
			m.NewMediaType("text", "poop"),
		})
}
