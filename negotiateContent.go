package mediatype

import "sort"

func (amts MediaTypes) Negotiate(mts []MediaType) MediaType {
	sort.Sort(byPriority(amts))

	for _, amt := range amts {
		for _, mt := range mts {
			if amt.match(mt) {
				return mt
			}
		}
	}

	return MediaType{}
}

func NegotiateContent(s string, mts MediaTypes) MediaType {
	amts, _ := ParseMediaTypes(s)
	return amts.Negotiate(mts)
}

func (mt1 MediaType) match(mt2 MediaType) bool {
	wc := "*"
	typeMatch := mt1.t == mt2.t || mt1.t == wc || mt2.t == wc
	subtypeMatch := mt1.s == mt2.s || mt1.s == wc || mt2.s == wc
	return typeMatch && subtypeMatch
}

type byPriority []MediaType

func (p byPriority) Len() int {
	return len(p)
}

func (p byPriority) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p byPriority) Less(i, j int) bool {
	return p[i].Quality() > p[j].Quality()
}
