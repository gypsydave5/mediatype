package mediatype

var Any = NewMediaType("*", "*")
var ApplicationJSON = NewMediaType("application", "json")
var ApplicationXML = NewMediaType("application", "xml")
var ImageSvg = NewMediaType("image", "svg+xml")
var TextHTML = NewMediaType("text", "html")
var TextPlain = NewMediaType("text", "plain")
