package mediatype

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type Type = string
type Subtype = string

type MediaTypes []MediaType
type MediaType struct {
	t Type
	s Subtype
	p map[string]string
}

func (mt MediaType) Parameters() map[string]string {
	return mt.p
}

func (mt MediaType) Type() Type {
	return mt.t
}

func (mt MediaType) Subtype() Subtype {
	return mt.s
}

func (mt MediaType) Quality() float32 {
	qs, ok := mt.p["q"]
	if !ok {
		return 1.0
	}
	q, _ := strconv.ParseFloat(qs, 32)
	return float32(q)
}

var NoMatch MediaType = MediaType{}

func NewMediaType(t Type, s Subtype) MediaType {
	return MediaType{
		t: t,
		s: s,
		p: newParameters(""),
	}
}

func newParameters(s string) map[string]string {
	result := make(map[string]string)
	if s == "" {
		return result
	}
	first := strings.Split(s, ";")
	for _, p := range first {
		ps := strings.Split(p, "=")
		if len(ps) != 2 {
			break
		}
		result[ps[0]] = ps[1]
	}
	return result
}

func ParseMediaType(s string) (MediaType, error) {
	split := strings.Split(s, "/")
	var err error
	if len(split) != 2 {
		err = fmt.Errorf("Deformed media type: %s", s)
	}

	secondSplit := strings.SplitN(split[1], ";", 2)
	var params string
	t, st := split[0], secondSplit[0]
	if len(secondSplit) == 2 {
		params = secondSplit[1]
	} else {
		params = ""
	}
	fmt.Println(params)
	return MediaType{t, st, newParameters(params)}, err
}

func ParseMediaTypes(s string) (MediaTypes, error) {
	var mts MediaTypes
	var err error

	types := strings.Split(s, ",")
	for _, t := range types {
		mt, err := ParseMediaType(t)
		if err != nil {
			break
		}
		mts = append(mts, mt)
	}

	sort.Sort(byPriority(mts))
	return mts, err
}

func NewAcceptMediaType(t Type, s Subtype, q float32) MediaType {
	return MediaType{
		t: t,
		s: s,
		p: map[string]string{"q": fmt.Sprintf("%f", q)},
	}
}
func NewMediaTypeWithParams(t Type, s Subtype, p map[string]string) MediaType {
	return MediaType{
		t: t,
		s: s,
		p: p,
	}
}

func (mt MediaType) String() string {
	return fmt.Sprintf("%s/%s", mt.t, mt.s)
}

func parseQualityValue(s string) (float32, error) {
	qs := strings.Split(s, "=")
	if len(qs) != 2 {
		return 0.0, fmt.Errorf("Unexpected Quality Value in %v", s)
	}

	qv, err := strconv.ParseFloat(qs[1], 32)
	if err != nil {
		return 0.0, fmt.Errorf("Cannot parse Quality Value as a float32:  %v", s)
	}

	return float32(qv), nil
}
